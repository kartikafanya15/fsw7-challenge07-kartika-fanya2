import React from 'react';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
import Cars from '../pages/Cars';

const MainLayout = (props) => {
    return (
        <>
            <Navbar/>
            {props.children}
            <Footer/>
        </>
    );
}

export default MainLayout;
