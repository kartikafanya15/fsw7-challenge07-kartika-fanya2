import React, {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Hero from '../components/Hero';
import MainLayout from '../layouts/MainLayout';
import { getCars, searchCars } from '../store/actions';

const Cars = () => {
   
    const data = useSelector(state => state.data.data)
    const dispatch = useDispatch()

    const [tanggal,setTanggal] = useState('')
    const [waktu,setWaktu] = useState('')
    const [kapasitas,setKapasitas] = useState('')

    const handleTanggal = event => {
        setTanggal(event.target.value)
    }

    const handleWaktu= event => {
        setWaktu(event.target.value)
    }
    
    const handleKapasitas= event => {
        setKapasitas(event.target.value)
    }

    const handleSubmit= () => {
        dispatch(searchCars(item => { 
            let inputTanggal =Date.parse(tanggal + "T" + waktu);
            let availabelDateVal = Date.parse(item.availableAt)
            let penumpangVal =item.capacity

            let result = true;

            if (inputTanggal && availabelDateVal){
                result = (availabelDateVal >= inputTanggal) && (penumpangVal >= kapasitas);
            }else if(inputTanggal){
                result = availabelDateVal >= inputTanggal
            }else if(Number(kapasitas)){
                result = item.capacity > Number(kapasitas)
            }

            return result;
        }))
    }

    useEffect(() => {
        dispatch(getCars())
    }, []);

    return (
        <>
            <MainLayout>
                <Hero />
                <section id="searchCars">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-xl-10">
                                <div class="search-bar p-4 shadow bg-white">
                                        <div class="row">
                                            <div class="col-12 col-lg-10">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="mt-3">
                                                            <label for="select-driver" class="form-label text-muted">Tipe Driver</label>
                                                            <select class="form-select" aria-label="Default select example"
                                                                id="tipeDriver">
                                                                <option selected disabled>Pilih Tipe Driver</option>
                                                                <option value="1">Dengan Supir</option>
                                                                <option value="2">Tanpa Supir</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="mt-3">
                                                            <label for="date" class="form-label text-muted">Tanggal</label>
                                                            <input type="date" class="form-control" id="dateSewa" name="date" onChange={(value) => handleTanggal(value)}  />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="mt-3">
                                                            <label for="pick-up" class="form-label text-muted">Waktu Jemput/Ambil</label>
                                                            <select class="form-select" aria-label="Default select example" name="time"
                                                                id="waktuJemput" onChange={(value) => handleWaktu(value)} >
                                                                <option selected disabled>Pilih Waktu</option>
                                                                <option value="08:00">08:00 &emsp;&emsp; WIB</option>
                                                                <option value="09:00">09:00 &emsp;&emsp; WIB</option>
                                                                <option value="10:00">10:00 &emsp;&emsp; WIB</option>
                                                                <option value="11:00">11:00 &emsp;&emsp; WIB</option>
                                                                <option value="12:00">12:00 &emsp;&emsp; WIB</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="mt-3">
                                                            <label for="total" class="form-label text-muted">Jumlah Penumpang (opsional)</label>
                                                            <input type="number" class="form-control" name="total-passenger" id="jumlahpenumpang" min="0" onChange={(value) => handleKapasitas(value)}  />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12 col-lg-2 position-relative mt-5 pt-4">
                                                <button class="btn btn-pos position-absolute bottom-0 text-white fw-bolder btn-success" id="submitFilter" onClick={() => handleSubmit()}>Cari Mobil</button>
                                            </div>

                                        </div>
                                    
                                </div>
                                <div class="row mt-5">
                                    {data.map(item => 
                                        <div class="col-12 col-lg-4 col-md-6 mb-3">
                                            <div class="card">
                                                <img src= {item.image.replace("./" , "https://res.cloudinary.com/dsacdaha8/image/upload/v1653572871/")}class="card-img-top" alt={item.image}/>
                                                <div class="card-body">
                                                    <h5>{item.manufacture + "/" + item.type}</h5>
                                                    <h5 class="card-title">Rp {item.rentPerDay} /hari</h5>
                                                    <p class="card-text">{item.description}</p>
                                                    <p>{item.capacity}</p>
                                                    <p>{item.transmission}</p>
                                                    <p>{item.year}</p>
                                                    <button class="btn btn-success w-100">Pilih Mobil</button>
                                                </div>
                                            </div>
                                        </div>  
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </MainLayout>
        </>
    );
}

export default Cars;