import axios from "axios";
export const GET_CARS = 'GET_CARS';
export const SEARCH_CARS = 'SEARCH_CARS';

export const getCars = () => (dispatch) => {
    axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
    .then(data => dispatch({
        type: GET_CARS,
        payload: data.data
    }))
}

export const searchCars = (condition) => async (dispatch) => {
    axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
    .then(data => dispatch({
        type: SEARCH_CARS,
        payload: data.data.filter(condition)
    }))
}

    